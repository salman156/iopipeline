#pragma once
#include <vector>
#include "channel.hpp"
#include "thread_pool.hpp"
#include "ip_packet.hpp"
#include "request_hierarchy.hpp"
#include "request_id.hpp"

namespace io_pipeline
{
    template<typename In, typename Out, Out(*Func)(std::vector<In>*), bool delete_packet = true>
    class demultiplexer_block{

        Out (*work_function)(std::vector<In>* param) = Func;
        channel next;

        static void demultiplexer_func(demultiplexer_block* self, unsigned long long id){
            In param = ip_packet<In>::get(id);
            auto [count, parent_id] = request_hierarchy::get_parent_id(id);
            std::vector<In>* list = nullptr;
            if(!ip_packet<std::vector<In>*>::exists(parent_id)){
                list = new std::vector<In>(count+1);
                list->at(count) = param;
                ip_packet<std::vector<In>*>::insert(parent_id, list);
            }
            else{
                list = ip_packet<std::vector<In>*>::get(parent_id);
                list->at(count) = param;
            }
            if(count == 0){
                Out res = self->work_function(list);
                ip_packet<Out>::insert(parent_id, res);
                self->next.submit(parent_id);
            }
            if constexpr(delete_packet){
                ip_packet<In>::erase(id);
            }
        }
        public:
        demultiplexer_block(int parallelism=4, int tasks_size=2048) : 
        pool(parallelism, tasks_size), next(nullptr){
            pool.set_object(this);
        }

        concurrent::thread_pool<demultiplexer_block*, demultiplexer_func> pool;
        
        void start(){
            pool.start();
        }
        void set_outgoing_channel(channel _next){
            next = _next;
        }
        channel get_channel(){
            return channel(pool.get_channel());
        }
        
    };

} // namespace io_pipeline
