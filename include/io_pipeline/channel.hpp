#pragma once
#include "threadsafe_queue.hpp"

namespace io_pipeline{
    class channel{
        concurrent::threadsafe_queue<unsigned long long>* next;
        public:
        channel(){ next = nullptr;}
        channel(concurrent::threadsafe_queue<unsigned long long>* _next) : next(_next){};
        void submit(unsigned long long id){
            next->queue_put(id);
        }
    };
}