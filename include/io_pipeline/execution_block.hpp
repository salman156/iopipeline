#pragma once
#include "channel.hpp"
#include "thread_pool.hpp"
#include "ip_packet.hpp"
#include "request_hierarchy.hpp"
#include "request_id.hpp"

namespace io_pipeline
{
    template<typename In, typename Out, Out(*Func)(In), bool delete_packet = false>
    class execution_block{

        Out (*work_function)(In) = Func;
        channel next;

        static void block_func(execution_block* self, unsigned long long id){
            In param = ip_packet<In>::get(id);
            Out res = self->work_function(param);
            ip_packet<Out>::insert(id,res);
            if constexpr(delete_packet){
                ip_packet<In>::erase(id);
            }
            self->next.submit(id);
        }
        public:
        execution_block(int parallelism=4, int tasks_size=2048) : 
        pool(parallelism, tasks_size), next(nullptr){
            pool.set_object(this);
        }

        concurrent::thread_pool<execution_block*, block_func> pool;
        
        void start(){
            pool.start();
        }
        void set_outgoing_channel(channel _next){
            next = _next;
        }
        channel get_channel(){
            return channel(pool.get_channel());
        }
        
    };

} // namespace io_pipeline
