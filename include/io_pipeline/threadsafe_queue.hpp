#pragma once
#include <mutex>
#include <condition_variable>

namespace concurrent {

    template<typename T>
    class threadsafe_queue{
        T* buffer;
        unsigned int in;
        unsigned int out;
        int capacity;
        unsigned int size;
        std::mutex q_mutex;
        std::condition_variable cond_full;
        std::condition_variable cond_empty;
    public:

        threadsafe_queue(int sz){
            buffer = new T[sz];
            capacity = sz;
            size = 0;
            in = 0;
            out = 0;
        }

        void queue_put(T value){
            std::unique_lock lock(q_mutex);

            cond_full.wait(lock, [this] { return size != capacity; });

            buffer[in] = value;
            in = (in + 1) % capacity;
            size++;

            cond_empty.notify_one();
        }

        void queue_get(T& value){
            std::unique_lock lock(q_mutex);

            cond_empty.wait(lock, [this] { return size > 0;});
            value = buffer[out];
            out = (out + 1) % capacity;
            size--;
            cond_full.notify_one();
        }
        
        int queue_try_get(T& value){
            std::unique_lock lock(q_mutex, std::defer_lock);
            bool locked;

            if(locked = lock.try_lock(); locked){
                cond_empty.wait(lock,[this] { return size > 0;});
                value = buffer[out];
                out = (out + 1) % capacity;
                size--;
                cond_full.notify_one();
            }

            return locked;
        }

        int queue_try_put(T value){
            std::unique_lock lock(q_mutex, std::defer_lock);
            int locked;
            
            if(locked = lock.try_lock(); locked){
                cond_full.wait(lock,[this] { return size < capacity;});
                buffer[in] = value;
                in = (in + 1) % capacity;
                size++;
                cond_empty.notify_one();
            }

            return locked;
        }

        int queue_size(){
            std::unique_lock lock(q_mutex);

            return size;
        }

        ~threadsafe_queue(){
            delete[] buffer;
        }
    };




}