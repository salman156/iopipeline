#pragma once
#include "channel.hpp"
#include "thread_pool.hpp"
#include "ip_packet.hpp"
#include "request_hierarchy.hpp"
#include "request_id.hpp"

namespace io_pipeline
{
    template<typename In, void(*Func)(In)>
    class end_block{

        void (*work_function)(In) = Func;

        static void end_func(end_block* self, unsigned long long id){
            In res = ip_packet<In>::get(id);
            self->work_function(res);
            ip_packet<In>::erase(id);
        }

        concurrent::thread_pool<end_block*, end_func> pool;
    public:
        end_block(int parallelism=4, int tasks_size=2048) : 
        pool(parallelism, tasks_size) {
            pool.set_object(this);
        }
        void start(){
            pool.start();
        }
        channel get_channel(){
            return {pool.get_channel()};
        }
    };

} // namespace io_pipeline
