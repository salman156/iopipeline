#pragma once
#include<atomic>

namespace io_pipeline{
    std::atomic<unsigned long long> id_counter;
    class request_id{
    public:
        static unsigned long long new_id(){
            return id_counter.fetch_add(1);
        }

    };
}