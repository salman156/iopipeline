#pragma once
#include "channel.hpp"
#include "eternal_thread_pool.hpp"
#include "ip_packet.hpp"
#include "request_hierarchy.hpp"
#include "request_id.hpp"

namespace io_pipeline
{
    template<typename Out, Out(*Func)()>
    class start_block{

        Out (*work_function)() = Func;
        channel next;

        static void start_func(start_block* self){
            Out res = self->work_function();
            ip_packet<Out>::insert(id,res);
            self->next.submit(id);
        }

        concurrent::eternal_thread_pool<start_block*, start_func> pool;
    public:
        start_block(int parallelism=4, int tasks_size=2048) : 
        pool(parallelism, tasks_size), next(nullptr){
            pool.set_object(this);
        }
        void set_outgoing_channel(channel _next){
            next = _next;
        }
        void start(){
            pool.start();
        }
        channel get_channel(){
            return {pool.get_channel()};
        }
    };

} // namespace io_pipeline
