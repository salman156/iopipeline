#pragma once
#include "threadsafe_queue.hpp"
#include <thread>
#include <mutex>
#include <condition_variable>
#include <vector>

namespace concurrent{

    template<typename Self, void(Func)(Self, unsigned long long)>
    class thread_pool{
        threadsafe_queue<unsigned long long> work_queue;
        std::vector<std::thread> threads;
        Self object;
        void (*work_function)(Self, unsigned long long) = Func;

        bool done;
        void worker_thread(){
            while(!done){
                unsigned long long param;
                work_queue.queue_get(param);
                work_function(object, param);
            };
        };
        
        public:
        void set_object(Self obj){
            object = obj;
        };
        thread_pool(int sz, int tasks_size) : work_queue(tasks_size){
            done = false;
            threads.reserve(sz);
        };
        void start(){
            for(int i = 0; i < threads.capacity(); i++){
                threads.emplace_back(std::thread(worker_thread, this));
            }
        };
        void submit(unsigned long long param){
            work_queue.queue_put(param);
        };
        threadsafe_queue<unsigned long long>* get_channel(){
            return &work_queue;
        }
    };

}