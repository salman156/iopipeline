#pragma once
#include <shared_mutex>
#include <mutex>
#include <unordered_map>

namespace io_pipeline{
    template<typename T>
    std::unordered_map<unsigned long long, T> packets;
    template<typename T>
    std::shared_mutex ip_mutex;
    
    template<typename T>
    class ip_packet{
        public:
            static void insert(unsigned long long request_id, T packet){
                std::unique_lock lock(ip_mutex<T>);
                packets<T>[request_id]=packet;
            };
            static void erase(unsigned long long request_id){
                std::unique_lock lock(ip_mutex<T>);
                packets<T>.erase(request_id);
            };
            static bool exists(unsigned long long request_id){
                std::shared_lock lock(ip_mutex<T>);
                return packets<T>.find(request_id) != packets<T>.end();
            }
            static T& get(unsigned long long request_id){
                std::shared_lock lock(ip_mutex<T>);
                return packets<T>[request_id];
            };
    };
}