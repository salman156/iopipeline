#pragma once
#include <shared_mutex>
#include <unordered_map>
#include <mutex>
#include <vector>
namespace io_pipeline{
    std::unordered_map<unsigned long long, std::pair<unsigned long long, unsigned long long>> requests;
    std::mutex s_mutex;
    class request_hierarchy{
        static void set_root_id(unsigned long long id){
            requests.insert({id, {0, 0}});
        }
    public:
        static std::pair<unsigned long long, unsigned long long> get_parent_id(unsigned long long id){
            std::unique_lock lock(s_mutex);
            unsigned long long parent_id = requests[id].second;
            requests[parent_id].first--;
            return requests[parent_id];
        }
        static void set_parent(unsigned long long parent_id, unsigned long long id){
            std::unique_lock lock(s_mutex);
            if(requests.find(parent_id) == requests.end())
                set_root_id(parent_id);
            requests.insert({id, {0, parent_id}});
            requests[parent_id].first++;
        }
        static void erase_id(unsigned long long id){
            std::unique_lock lock(s_mutex);
            requests.erase(id);
        }

    };
}