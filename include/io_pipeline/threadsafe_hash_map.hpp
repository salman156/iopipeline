#pragma once
#include<atomic>

namespace concurrent{
    template<typename TKey, typename TValue>
    class threadsafe_hash_map{
        class bucket{
            struct node{
                node* next;
                TKey key;
                TValue value;
            };
            node *head;
            std::atomic<bool> done;
            void release(){
                bool free = true;
                while(!done.compare_exchange_weak(free, false));
            };
            void acquire(){
                bool free = false;
                while(!done.compare_exchange_weak(free, true));
            };
        public:
            void insert(TKey key, TValue value){
                acquire();
                node* temp = new node();
                temp->key = key;
                temp->value = value;
                temp->next = head;
                head = temp;
                release();
            };
            void erase(TKey key){
                acquire();
                node* temp = head;
                if(!temp){
                    release();
                    return;
                }
                if(head->key == key){
                    head = head->next;
                    delete temp;
                    release();
                    return;
                }
                while(temp->next != nullptr && temp->next->key !=key){
                    temp = temp->next;
                }
                if(temp->next){
                    node* delete_node = temp->next;
                    temp->next = temp->next->next;
                    delete delete_node;
                }
                release();

            };
            std::pair<TValue,bool> find(TKey key){
                acquire();
                node* temp = head;
                if(!temp){
                    release();
                    return {{}, false};
                }
                if(head->key == key){
                    release();
                    return {head->value,true};
                }
                while(temp->next != nullptr && temp->next->key != key){
                    temp = temp->next;
                }
                if(temp->next){
                    release();
                    return {temp->next->value,true};
                }
                release();
                return {{}, false};
            };
        };
        bucket* head;
        int capacity;
    public:
        threadsafe_hash_map(int size){
            head = new bucket[size];
            capacity = size;
        };
        void insert(TKey key, TValue value){
            head[key%capacity].insert(key, value);
        };
        void erase(TKey key){
            head[key%capacity].erase(key);
        };
        TValue& find(TKey key){
            head[key%capacity].find(key);
        };
    };
}