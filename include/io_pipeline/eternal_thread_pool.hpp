#include <thread>
#include <vector>

namespace concurrent{
    template<typename Self, void(Func)(Self)>
    class eternal_thread_pool{
        std::vector<std::thread> threads;
        Self object;
        void (*work_function)(Self) = Func;

        bool done;
        void worker_thread(){
            while(!done){
                work_function(object);
            };
        };
        
        public:
        void set_object(Self obj){
            object = obj;
        };
        eternal_thread_pool(int sz, int tasks_size) : work_queue(tasks_size){
            done = false;
            threads.reserve(sz);
        };
        void start(){
            for(int i = 0; i < threads.capacity(); i++){
                threads.emplace_back(std::thread(worker_thread, this));
            }
        };
    };
}