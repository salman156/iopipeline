#pragma once
#include "start_block.hpp"

namespace io_pipeline{
    template<typename In>
    class pipeline{

         start;
    
    public:
        pipeline() : start(){
        };
        
        template<typename ResultType, typename ArgType>
        execution_block* GetBuilder(ResultType (*func)(ArgType)){
            start.set_workingfunction<pipeline_block::block_type::start_block>(func);
            return &start;
        }

        void run(){
            start.run();
        }
    };
}