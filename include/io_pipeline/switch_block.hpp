#pragma once
#include "channel.hpp"
#include "thread_pool.hpp"
#include "ip_packet.hpp"
#include "request_hierarchy.hpp"
#include "request_id.hpp"

namespace io_pipeline
{
    template<typename In, int(*Func)(In), int case_count = 2, bool delete_packet = false>
    class switch_block{

        int (*work_function)(In) = Func;
        channel* next;

        static void switch_func(switch_block* self, unsigned long long id){
            In param = ip_packet<In>::get(id);
            int res = self->work_function(param);
            if constexpr(delete_packet){
                ip_packet<In>::erase(id);
            }
            self->next[res].submit(id);
        }

        concurrent::thread_pool<switch_block*, switch_func> pool;
    public:
        switch_block(int parallelism=4, int tasks_size=2048) : 
        pool(parallelism, tasks_size), next(nullptr) {
            next = new channel[case_count];
            for(int i = 0; i < case_count; i++)
                next[i] = channel(nullptr);
            pool.set_object(this);
        }
        void start(){
            pool.start();
        }
        void set_outgoing_channel(int _case, channel _next){
            next[_case] = _next;
        }
        channel get_channel(){
            return {pool.get_channel()};
        }
    };

} // namespace io_pipeline
