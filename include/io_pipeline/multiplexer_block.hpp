#pragma once
#include <vector>
#include "channel.hpp"
#include "thread_pool.hpp"
#include "ip_packet.hpp"
#include "request_hierarchy.hpp"
#include "request_id.hpp"

namespace io_pipeline
{
    template<typename In, typename Out, std::vector<Out>(*Func)(In), bool delete_packet = false>
    class multiplexer_block{

        std::vector<Out> (*work_function)(In) = Func;
        channel next;

        static void multiplexer_func(multiplexer_block* self, unsigned long long id){
            In param = ip_packet<In>::get(id);
            std::vector<Out> res = self->work_function(param);
            for(int i = 0; i < res.size(); i++){
                unsigned long long child_id = request_id::new_id();
                request_hierarchy::set_parent(id, child_id);
                ip_packet<Out>::insert(child_id,res[i]);
                self->next.submit(child_id);
            }
            if constexpr(delete_packet){
                ip_packet<In>::erase(id);
            }
        }
        public:
        multiplexer_block(int parallelism=4, int tasks_size=2048) : 
        pool(parallelism, tasks_size), next(nullptr){
            pool.set_object(this);
        }

        concurrent::thread_pool<multiplexer_block*, multiplexer_func> pool;
        
        void start(){
            pool.start();
        }
        void set_outgoing_channel(channel _next){
            next = _next;
        }
        channel get_channel(){
            return channel(pool.get_channel());
        }
        
    };

} // namespace io_pipeline
