#include<iostream>
#include<vector>
#include<mutex>
#include "../include/io_pipeline/start_block.hpp"
#include "../include/io_pipeline/execution_block.hpp"
#include "../include/io_pipeline/multiplexer_block.hpp"
#include "../include/io_pipeline/demultiplexer_block.hpp"
#include "../include/io_pipeline/switch_block.hpp"
#include "../include/io_pipeline/end_block.hpp"

double l = 1;
const int N = 81;
int counter = 1000;
std::vector<std::vector<double>> A(N,std::vector<double>(N));
std::vector<std::vector<double>> A_copy(N,std::vector<double>(N));
std::vector<std::vector<double>> A_tochn(N,std::vector<double>(N));
const int p_num = 9;
const double h = l / N;
double eps = 0.1;
double resto = 0.0;
std::mutex done;

double phi(double x, double y){
    return x*x + y*y;
}
double f(double x, double y){
    return 4;
}
double x_0(double x, double y){
    return 100;
}

int init(){
    for(int i = 0; i < N; i++)
        for(int j = 0; j < N; j++)
            A_tochn[i][j] = phi(i*h, j*h);

    for(int i = 0; i < N; i++)
        for(int j = 0; j < N; j++)
            A[i][j] = x_0(i*h,j*h);

    for(int i = 0; i < N; i++){
        A[0][i] = phi(i*h,0);
        A[i][0] = phi(0,i*h);
        A[N-1][i] = phi(i*h,l);
        A[i][N-1] = phi(l,i*h);
    }
        
    const int p = 9;
    return p;
}

std::vector<int> multiplex(int proc_num){
    std::vector<int> procs(proc_num);

    for(int i =0; i < proc_num; i++)
        procs[i] = i;

    return procs;
}

double jacobi_proc(int proc_num){
    int kolvo = N / 3;
    int x_start = proc_num % 3;
    int y_start = proc_num / 3;

    for(int i = y_start*kolvo; i < y_start*kolvo + kolvo; i++)
        for(int j = x_start*kolvo; j < x_start*kolvo + kolvo; j++)
            A_copy[i][j] = A[i][j];

    for(int i = y_start*kolvo; i < y_start*kolvo + kolvo; i++)
        for(int j = x_start*kolvo; j < x_start*kolvo + kolvo; j++)
            if(i != 0 && j !=0 && i != N-1 && j != N-1)
                A[i][j] = -0.25*h*h*f(j*h,i*h)+0.25*(A_copy[i][j+1]+A_copy[i][j-1]+A_copy[i+1][j]+A_copy[i-1][j]);
    
    double res = 0.0;
    for(int i = y_start*kolvo; i < y_start*kolvo + kolvo; i++)
        for(int j = x_start*kolvo; j < x_start*kolvo + kolvo; j++)
            res += (A[i][j]-A_tochn[i][j])*(A[i][j]-A_tochn[i][j]);

    return res;
}

double demultiplex(std::vector<double>* eps){
    double res = 0.0;
    for(int i = 0; i < eps->size(); i++)
            res += eps->at(i);
    counter--;
    return res;
}

int switch_func(double s){
    if(s < eps || counter == 0)
        return 0;
    else
        return 1;
}
void end(double res){
    resto = res;
    done.unlock();
}

int main(){
    done.lock();
    auto start_block = new io_pipeline::start_block<int,init>(1,1);
    auto multiplexer_block = new io_pipeline::multiplexer_block<int,int,multiplex>;
    auto parallel_block = new io_pipeline::execution_block<int, double,jacobi_proc>(9);
    auto demultiplexer_block = new io_pipeline::demultiplexer_block<double,double,demultiplex>;
    auto switch_block = new io_pipeline::switch_block<double,switch_func,2>(1);
    auto done_block = new io_pipeline::end_block<double,end>(1);
    start_block->set_outgoing_channel(multiplexer_block->get_channel());
    multiplexer_block->set_outgoing_channel(parallel_block->get_channel());
    parallel_block->set_outgoing_channel(demultiplexer_block->get_channel());
    demultiplexer_block->set_outgoing_channel(switch_block->get_channel());
    switch_block->set_outgoing_channel(0,done_block->get_channel());
    switch_block->set_outgoing_channel(1,multiplexer_block->get_channel());
    start_block->start();
    multiplexer_block->start();
    parallel_block->start();
    demultiplexer_block->start();
    switch_block->start();
    done_block->start();
    auto start = std::chrono::high_resolution_clock::now();
    start_block->get_channel().submit(io_pipeline::request_id::new_id());
    done.lock();
    auto end = std::chrono::high_resolution_clock::now();

    std::cout << " time: " << std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();

    for(int i = 0; i < N; i++)
        for(int j = 0; j < N; j++)
            A[i][j] = x_0(i*h,j*h);

    for(int i = 0; i < N; i++){
        A[0][i] = phi(i*h,0);
        A[i][0] = phi(0,i*h);
        A[N-1][i] = phi(i*h,l);
        A[i][N-1] = phi(l,i*h);
    }
    int iter = 1000;
    start = std::chrono::high_resolution_clock::now();
    while(iter--){
        for(int i = 0; i < N; i++)
            for(int j = 0; j < N; j++)
                A_copy[i][j] = A[i][j];

        for(int i = 0; i < N; i++)
            for(int j = 0; j < N; j++)
                if(i != 0 && j !=0 && i != N-1 && j != N-1)
                    A[i][j] = -0.25*h*h*f(j*h,i*h)+0.25*(A_copy[i][j+1]+A_copy[i][j-1]+A_copy[i+1][j]+A_copy[i-1][j]);
        
        double res_real = 0.0;
        for(int i = 0; i < N; i++)
            for(int j = 0; j < N; j++)
                res_real += (A_tochn[i][j] - A[i][j])*(A_tochn[i][j] - A[i][j]);
        
        if(res_real <eps)
            break;
    }
    end = std::chrono::high_resolution_clock::now();
    std::cout << std::endl << "time: " << std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
}