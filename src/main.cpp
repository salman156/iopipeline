#include<iostream>
#include<chrono>
#include "../include/io_pipeline/start_block.hpp"
#include "../include/io_pipeline/execution_block.hpp"
#include "../include/io_pipeline/switch_block.hpp"
#include "../include/io_pipeline/end_block.hpp"



int test(){
    
}
int test2(int a){
    return a*a;
}
std::mutex mut;
void greater(int a){
    std::unique_lock lock(mut);
    std::cout<< "Число "<< a << " больше 100";
}
void lesser(int a){
    std::unique_lock lock(mut);
    std::cout<< "Число "<< a << " меньше 100";
}
int case_func(int a){
    if(a > 100){
        return 0;
    }
    else
        return 1;
}

int main(){
    using namespace std::chrono_literals;
    auto start_block = new io_pipeline::start_block<int,test>;
    auto next_block = new io_pipeline::execution_block<int,int,test2>;
    auto switch_block = new io_pipeline::switch_block<int,case_func>;
    auto end_block1 = new io_pipeline::end_block<int,greater>;
    auto end_block2 = new io_pipeline::end_block<int,lesser>;
    start_block->set_outgoing_channel(next_block->get_channel());
    next_block->set_outgoing_channel(switch_block->get_channel());
    switch_block->set_outgoing_channel(0,end_block1->get_channel());
    switch_block->set_outgoing_channel(1,end_block2->get_channel());
    start_block->start();
    next_block->start();
    end_block1->start();
    end_block2->start();
    switch_block->start();
    io_pipeline::ip_packet<int>::insert(0,9);
    io_pipeline::ip_packet<int>::insert(1,12);
    start_block->get_channel().submit(0);
    start_block->get_channel().submit(1);
    std::this_thread::sleep_for(10000000ms);
    //start_block->get_channel().submit(1);
}